# This may seem like a crazy way to install, but the instructions mentioned on
# mtail GH repo simply doesn't work, and the troubleshooting instructions are
# ridiculous (run make and if it fails go run make somewhere else then run the
# first make again!). So here I follow the way it's being installed/tested on their CI.

gopath    = "#{node['gitlab-mtail']['install_dir']}/gopath"
mtail_dir = "#{gopath}/src/github.com/google/mtail"

apt_update 'apt-get update' do
  action :update
end if node['platform_family'] == 'debian'

package 'golang'

directory gopath do
  recursive true
end

directory node['gitlab-mtail']['progs_dir']

ark 'mtail' do
  url node['gitlab-mtail']['release_url']
  path File.dirname(mtail_dir)
  owner node['gitlab-mtail']['user']
  group node['gitlab-mtail']['group']

  action :put
end

execute 'make' do
  cwd mtail_dir
  environment({
    'GOPATH' => gopath,
    'PATH' => "#{gopath}/bin:#{ENV['PATH']}",
  })
end

include_recipe 'runit::default'
runit_service 'mtail' do
  options ({
    binary_path: "#{gopath}/bin/mtail"
  })
  default_logger true
end
