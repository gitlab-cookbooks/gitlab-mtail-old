include_recipe 'gitlab-mtail::default'

cookbook_file "#{node['gitlab-mtail']['progs_dir']}/gitlab_shell.mtail" do
  source 'gitlab_shell.mtail'
  notifies :restart, 'runit_service[mtail]', :delayed
end
