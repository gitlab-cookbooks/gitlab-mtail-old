default['gitlab-mtail']['release_url'] = 'https://github.com/google/mtail/archive/a780a6342bd70a8fb8ffe187ef988d5417d43a96.tar.gz'
default['gitlab-mtail']['install_dir'] = '/opt/mtail'
default['gitlab-mtail']['progs_dir']   = "#{default['gitlab-mtail']['install_dir']}/progs"
default['gitlab-mtail']['log_paths']   = [] # Should be overridden by specialized recipes
default['gitlab-mtail']['user']        = 'git'
default['gitlab-mtail']['group']       = 'git'
