def format_log_paths(paths)
  case paths
  when Array
    paths.join(',')
  when String
    paths
  else
    ''
  end
end
